/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import annotation.Annotation;

/**
 *
 * @author mendrika
 */
public class Personne {
    String nom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @Annotation(url="getAll")
    public ModelView getAll(){
        ModelView mv = new ModelView();
        Personne[] p = new Personne[2];
        p[0] = new Personne();
        p[1] = new Personne();
        mv.add("personne", p);
        mv.setUrl("Personne.jsp");
        return mv;
    }
    
    @Annotation(url = "mampiditra")
    public ModelView mampiditra() {
        ModelView mv = new ModelView();
        mv.setUrl("Personne.jsp");
        System.out.println(this.getNom());
        return mv;
    }
}
